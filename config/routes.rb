Rails.application.routes.draw do

root 'homes#index'
get "admin_index" => 'houses#admin_index'
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

resources :houses do
	member do
		get :delete
	end
end

resources :users do
end


end
