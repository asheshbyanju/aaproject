class CreateHouses < ActiveRecord::Migration[5.1]
  def change
    create_table :houses do |t|
    	t.string "area"
    	t.string "type"
    	t.integer "rent"
    	t.string "size"
    	t.integer "room"
    	t.float "rental_period"
    	t.date "acquisition_date"
    	t.string "other"

      t.timestamps
    end
  end
end
