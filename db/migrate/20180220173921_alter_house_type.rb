class AlterHouseType < ActiveRecord::Migration[5.1]
  def change
  	rename_column("houses", "type", "product_type")
  end
end
