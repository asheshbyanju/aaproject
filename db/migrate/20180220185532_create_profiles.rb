class CreateProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :profiles do |t|
      t.string "first_name"
      t.string "last_name"
      t.text "email"
      t.text "address"
      t.text "country"
      t.integer "zip"
      t.integer "phone"
      t.timestamps
    end
  end
end
