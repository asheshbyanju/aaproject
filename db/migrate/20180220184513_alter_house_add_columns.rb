class AlterHouseAddColumns < ActiveRecord::Migration[5.1]
  def change
  	add_column("houses", "Address", :text)
  	add_column("houses", "advance_pay", :integer)
  	add_column("houses", "security_deposit", :integer)
  	add_column("houses", "prepaid_rent", :integer)
  	add_column("houses", "furnished", :boolean)
  	add_column("houses", "pets_allowed", :boolean)
  	add_column("houses", "title", :text)
  	add_column("houses", "description", :text)
  end
end
