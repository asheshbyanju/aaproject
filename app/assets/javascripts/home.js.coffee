$(document).on 'turbolinks:load', ->
  $('.carousel.carousel-slider').carousel({
    fullWidth: true
  })

  autoplay = ->
    $('.carousel').carousel 'next'
    setTimeout autoplay, 4500

  autoplay()

  $('.parallax').parallax()
