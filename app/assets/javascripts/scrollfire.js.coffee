$(document).on 'turbolinks:load', ->
  options = [
    {
      selector: '#home-scrollfire',
      offset: 300,
      callback: (el) ->
        Materialize.showStaggeredList $(el)
    }
  ]
  Materialize.scrollFire(options)