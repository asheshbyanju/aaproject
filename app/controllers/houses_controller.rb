class HousesController < ApplicationController
  
  def index
  @houses = House.all
  end

  def new
    @house = House.new
  end

  def create
    @house = House.create(house_params) 
    if @house.save
      flash[:notice] = "House Added Successfully"
      redirect_to(houses_path)
    else 
      render('new')
    end
  end

  def show
    @house = House.find(params[:id])
  end

  def edit
    @house = House.find(params[:id])
  end

  def update
    @house = House.find(params[:id])
    if @house.update_attributes(house_params)

    flash[:notice] = "House Updated Successfully"
    #redirect_to(pages_path)
    redirect_to(houses_path(@house))
    else
      render('edit')
    end
  end

  def delete
    @house = House.find(params[:id])
    @house.destroy
    flash[:notice] = "House Deleted Successfully"
    redirect_to(houses_path)
  end

  def admin_index
  @houses = House.all
  end

  def house_params
  params.require(:house).permit(:area, :type, :rent, :size, :room, :rental_period, :acquisition_date, :other)

  end
end
